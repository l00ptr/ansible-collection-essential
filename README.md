# Ansible Collection - dalibo.essential

A collection of Ansible roles for the **Dalibo PostgreSQL Platform**.

## Install

```console
ansible-galaxy collection install dalibo.essential
```

## Collection-level parameters

For the most part, each role in this collection has its own parameters, with the
name of the role as prefix (e.g. `pg_back_sys_user`). However, it is possible
to define some variables once for all roles.

At this time, only one collection-level parameter is available:

### dalibo_essential_packages_lock_state

Determines if the packages installed by this collection should be version-locked.
Available only for RHEL and clones. By default, the version lock policies are not
modified.

Choices:

- `absent` : unlock the versions of the packages
- `present`: lock the versions of the packages
