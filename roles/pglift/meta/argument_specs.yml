---
argument_specs:
  main:
    short_description: Install pglift
    description: Install and configure pglift.
    author: DALIBO
    options:
      pglift_settings_base_path:
        type: path
        description: Base directory for pglift configuration files and templates.
      pglift_lingering_mode:
        type: bool
        description: Define if pglift should be installed and configured with lingering
          enabled for the operator user. When this variable is set to true, the pglift
          configuration files are stored on the XDG_CONFIG_HOME directory (~/.config/pglift)
          by default.
      pglift_configure_shell_completion:
        type: bool
        description: Define if pglift shell (currently only available for bash) autocompletion
          should be configured.
      pglift_package:
        type: str
        description: Name of the pglift package the role should install.
      pglift_install_method:
        type: str
        description: Defines how pglift will be installed, either from pypi or from
          system repository. Consider pip installation of pglift only for testing
          purposes (it's not intended to be used in production system).
        choices:
          - pip
          - package
      pglift_settings_sys_user:
        type: str
        description: System user owning the configuration files for pglift by default.
      pglift_settings_sys_group:
        type: str
        description: System group owning the configuration files for pglift by default.
      pglift_operator_sys_user:
        type: str
        description: The user who will run/operate pglift.
      pglift_operator_sys_group:
        type: str
        description: Group of the user who will run/operate pglift.
      pglift_directories:
        type: list
        elements: path
        description: List of path where the role will create configuration or template.
          files for pglift.
      pglift_settings_dest:
        type: path
        description: Configuration file for pglift.
      pglift_settings_mode:
        type: str
        description: Mode (Linux permission) to apply to the pglift configuration
          files.
      pglift_settings_owner:
        type: str
        description: User owning the pglift configuration files.
      pglift_settings_group:
        type: str
        description: Group owning the pglift configuration files.
      pglift_settings_content:
        type: dict
        description: Configuration for pglift.
      pglift_postgresql_conf_dest:
        type: path
        description: Path to store PostgreSQL configuration templates for pglift.
      pglift_postgresql_conf_mode:
        type: str
        description: Mode (Linux permission) to apply to PostgreSQL configuration.
          templates.
      pglift_postgresql_conf_content:
        type: str
        description: Content of the PostgreSQL configuration template.
      pglift_pg_hba_conf_dest:
        type: path
        description: Path to pg_hba template.
      pglift_pg_hba_conf_mode:
        type: str
        description: Mode (Linux permission) to apply to the pg_hba template.
      pglift_pg_hba_conf_content:
        type: str
        description: Content of the pg_hba template.
      pglift_pg_ident_conf_dest:
        type: path
        description: Path to the pg_ident template for pglift.
      pglift_pg_ident_conf_mode:
        type: str
        description: Mode (Linux permission) to apply to the pg_ident template.
      pglift_pg_ident_conf_content:
        type: str
        description: Content of the pg_ident template.
      pglift_pgbackrest_conf_dest:
        type: path
        description: Path to the pgbackrest template configuration for pglift.
      pglift_pgbackrest_conf_mode:
        type: str
        description: Mode (Linux permission) to apply to the pgbackrest template.
      pglift_pgbackrest_conf_content:
        type: str
        description: Content of the pgbackrest template.
