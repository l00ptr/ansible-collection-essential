---
- name: Ensure ansible_facts used by role
  ansible.builtin.setup:
    gather_subset: min
  when: (not ansible_facts.keys() | list | intersect(__pglift_required_facts) ==
    __pglift_required_facts)

- name: Set install method (pip or package) specific variables
  ansible.builtin.include_vars: '{{ role_path }}/vars/{{ pglift_install_method }}.yml'

- name: Set platform/version specific variables
  ansible.builtin.include_vars: '{{ __pglift_vars_file }}'
  when: __pglift_vars_file is file
  loop:
    - "{{ ansible_facts['os_family'] }}.yml"
    - "{{ ansible_facts['distribution'] }}.yml"
    - "{{ ansible_facts['distribution'] }}_{{ ansible_facts['distribution_major_version']\
      \ }}.yml"
    - "{{ ansible_facts['distribution'] }}_{{ ansible_facts['distribution_version']\
      \ }}.yml"
  vars:
    __pglift_vars_file: '{{ role_path }}/vars/{{ item }}'

- name: Install pglift based on install method - {{ pglift_install_method }}
  ansible.builtin.include_tasks: '{{ pglift_install_method }}.yml'

- name: Test if pglift base path exist
  ansible.builtin.stat:
    path: '{{ pglift_settings_base_path }}'
  register: __conf_dir

# We don't recurse if the path exists to avoid overriding
# permission on dir content.
#
# Recurse is only used to create parent and subdir if the
# parent doesn't exist
- name: Create pglift base path config dir
  ansible.builtin.file:
    path: '{{ pglift_settings_base_path }}'
    state: directory
    recurse: "{{ not __conf_dir.stat.exists }}"
    owner: '{{ pglift_settings_sys_user }}'
    group: '{{ pglift_settings_sys_group }}'
    mode: '0750'

- name: Create pglift directorie {{ item.value }}
  ansible.builtin.file:
    path: '{{ item.value }}'
    state: directory
    owner: '{{ pglift_settings_sys_user }}'
    group: '{{ pglift_settings_sys_group }}'
    mode: '0750'
  with_dict: '{{ __pglift_directories }}'

- name: Enable linger for PostgreSQL user
  ansible.builtin.command:
    cmd: loginctl enable-linger {{ pglift_operator_sys_user }}
    creates: /var/lib/systemd/linger/{{ pglift_operator_sys_user }}
  when: pglift_lingering_mode

- name: Setting up configuration files
  ansible.builtin.import_tasks: configure_pglift.yml

- name: Add shell (bash) completion
  when: pglift_configure_shell_completion
  ansible.builtin.include_tasks: configure_shell_completion.yml
