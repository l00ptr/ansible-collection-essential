---
- name: configure | Creating pgbackrest directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    owner: "{{ pgbackrest_sys_user }}"
    group: "{{ pgbackrest_sys_group }}"
    mode: "{{ pgbackrest_mode }}"
    modification_time: preserve
    access_time: preserve
  loop:
    - "{{ pgbackrest_conf_directory }}"
    - "{{ pgbackrest_conf_directory_stanza }}"
    - "{{ pgbackrest_pki_directory | default(None) }}"
    - "{{ pgbackrest_log_directory | default(None) }}"
    - "{{ pgbackrest_lib_directory | default(None) }}"
  when: item|length>0

- name: configure | Managing certificates
  when: pgbackrest_global_certificates is defined
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "{{ item.owner | default(pgbackrest_sys_user) }}"
    group: "{{ item.group | default(pgbackrest_sys_group) }}"
    mode: "{{ item.mode | default('0600') }}"
  loop: "{{ pgbackrest_global_certificates }}"

- name: configure | Configure global pgbackrest.conf file
  ansible.builtin.template:
    src: pgbackrest-stanza.conf.j2
    dest: "{{ pgbackrest_global_conf_file }}"
    owner: "{{ pgbackrest_sys_user }}"
    group: "{{ pgbackrest_sys_group }}"
    mode: "0640"
  vars:
    pgbackrest_template_stanza_name: global
    pgbackrest_template_stanza_settings: "{{ pgbackrest_global_settings }}"
  when: pgbackrest_global_settings is defined

- name: configure | Manage stanza certificates
  ansible.builtin.copy:
    src: "{{ item.1.src }}"
    dest: "{{ item.1.dest }}"
    owner: "{{ item.1.owner | default(pgbackrest_sys_user) }}"
    group: "{{ item.1.group | default(pgbackrest_sys_group) }}"
    mode: "{{ item.1.mode | default('0600') }}"
  loop: "{{ pgbackrest_stanza_settings | ansible.builtin.subelements('certificates', skip_missing=true) }}"


- name: configure | Configure stanza pgbackrest.conf
  ansible.builtin.template:
    src: pgbackrest-stanza.conf.j2
    dest: "{{ pgbackrest_conf_directory_stanza }}/{{ item.name }}.conf"
    owner: "{{ pgbackrest_sys_user }}"
    group: "{{ pgbackrest_sys_group }}"
    mode: "0640"
  vars:
    pgbackrest_template_stanza_name: "{{ item.name }}"
    pgbackrest_template_stanza_settings: "{{ item.settings }}"
  loop: "{{ pgbackrest_stanza_settings }}"
  when: pgbackrest_stanza_settings is defined

- name: configure | Create pgbackrest.service.d directory
  ansible.builtin.file:
    path: /etc/systemd/system/pgbackrest.service.d/
    state: directory
    owner: root
    group: root
    mode: "0755"
  when: pgbackrest_server_service

- name: configure | Copy pgbackrest service override file
  ansible.builtin.template:
    src: pgbackrest-service-override.j2
    dest: /etc/systemd/system/pgbackrest.service.d/override.conf
    owner: root
    group: root
    mode: "0644"
  when: pgbackrest_server_service
  notify:
    - Restart pgbackrest service
