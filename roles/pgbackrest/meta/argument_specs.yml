---
argument_specs:
  main:
    short_description: Install pgbackrest
    description: |
      Install and configure pgbackrest either as a local backup agent or as
      a Dedicated Repository Host.
      This roles requires the PGDG and EPEL repository.
    author: Dalibo
    options:
      pgbackrest_conf_directory:
        type: path
        default: /etc/pgbackrest
        description: Which directory the pgbackrest.conf file will be located.

      pgbackrest_global_conf_file:
        type: path
        default: "{{ pgbackrest_conf_directory }}/pgbackrest.conf"
        description: Absolute path of the pgbackrest.conf file.

      pgbackrest_conf_directory_stanza:
        type: path
        default: "{{ pgbackrest_conf_directory }}/conf.d"
        description: Which directory the stanza.conf file will be located.

      pgbackrest_sys_user:
        type: str
        default: postgres
        description: >
          System user owner of all the files and directories created
          by this role.

      pgbackrest_sys_group:
        type: str
        default: postgres
        description: >
          System group owner of all the files and directories created
          by this role.

      pgbackrest_mode:
        type: str
        default: "0750"
        description: Permissions of the directories created by this role.

      pgbackrest_pki_directory:
        type: path
        description: Folder containing the pgbackrest certificates.

      pgbackrest_log_directory:
        type: path
        description: Folder containing the pgbackrest logs.

      pgbackrest_lib_directory:
        type: path
        description: Folder containing the pgbackrest libraries.

      pgbackrest_server_service:
        type: bool
        default: false
        description: Configure the pgbackrest service.

      pgbackrest_global_certificates:
        type: list
        description: All certificates fetch from the control node to the node.
        elements: dict
        options:
          src:
            type: path
            description: Path from the control node.

          dest:
            type: path
            description: Destination path onto the node.

          owner:
            type: str
            description: Owner user of the certificate.

          group:
            type: str
            description: Owner group of the certificate.

          mode:
            type: int
            description: Permissions to use when creating the certificate.

      pgbackrest_global_settings:
        type: str
        description: Global settings to use in pgbackrest.conf file.

      pgbackrest_stanza_settings:
        type: list
        description: >
            Stanza settings (can define multiple stanza configuration)
            in stanza.conf file.
        elements: dict
        options:
          name:
            type: str
            description: Name of the stanza.

          settings:
            type: str
            description: Actual settings of the stanza.

          certificates:
            type: list
            description: >
                is optional but if stanza's certificates are different
                from the global certificates.
            elements: dict
            options:
              src:
                type: path
                description: The path from the control node.

              dest:
                type: path
                description: the destination path onto the node.

              owner:
                type: str
                description: The owner of the certificate.

              group:
                type: str
                description: The owner group of the certificate.

              mode:
                type: str
                description: Permissions to use when creating the certificate.
