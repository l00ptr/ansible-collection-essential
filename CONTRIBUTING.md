# How To Contribute

## Setup a dev environment

Clone the git repository:

``` shell
git clone https://gitlab.com/dalibo/ansible-collection-essential/
cd ansible-collection-essential
```

Then, create a Python3 virtualenv and install the python toolset:

``` shell
python3 -m venv .venv --upgrade-deps
. .venv/bin/activate
pip install -r requirements-dev.txt
```

For the other commands below, we assume that the virtualenv is activated.

## Writing roles

### Variables

* All variables should have the name of the role as prefix.
* Variables that should not be changed (i.e. constant) belong
  in the `vars/main.yml` file
* Variables that can be overridden by the users belong in the
  `defaults/main.yml` file
* All variables in `defaults/main.yml` must be described in
  `meta/arguments_specs.yml`

### Modules

* Unless an OS specific action is required, always prefer
  the generic `ansible.builtin.package` rather than
  `ansible.builtin.dnf` or `ansible.builtin.apt`

* If you need to update packages list, update it separately using
  `ansible.builtin.dnf` for RedHat based OS or `ansible.builtin.apt` for Debian
  based OS.

* Avoid using `ansible.builtin.command` and `ansible.builtin.shell`
  as much as possible. If you really need them, don't forget to use `creates` or
  `changed_when` to ensure your task is idempotent.

### System packages

Except for specific cases, roles should not update the packages cache
themselves. This means that setting `update_cache: true` when using
`ansible.builtin.package` should be avoided.

## Changelog

For each new Merge Request, add a changelog file in the `changelogs/fragments/`
folder. For instance, if you modify a role named `foo`, then you should create
a fragment named `changelogs/fragments/foo.yaml` and describe the changes you
are introducing:

``` yaml
major_changes:
  - Introduce a new task named "init" to the foo role.

breaking_changes:
  - Version x.y.z of the bar package is no longer supported.

minor_changes:
  - Remove Typos in the documentation
```

A guide on how to write fragments is available in the
[Ansible development documentation].

[Ansible development documentation]: https://docs.ansible.com/ansible/devel/community/development_process.html#creating-a-changelog-fragment

## Linting

Check YAML syntax and do some Ansible validation of the files by running:

``` shell
yamllint roles
ansible-lint --offline roles
```

This commands can be run automatically before each git commit with `pre-commit`.
See below.

## Git pre-commit hook

We maintain a [pre-commit] configuration to operate some verification at commit
time, if you want to use that configuration you should:

- Install pre-commit (On Debian based system you can probably simply run :
  `sudo apt install pre-commit`)
- Then apply the configuration with `pre-commit install`
- And finally you can verify the configuration is properly applied by running
  it "by hand": `.git/hooks/pre-commit`

## Testing

Run all tests with:

``` shell
molecule test --all
```

Or, you can run only one role (ie. scenario) on one specific OS (ie. platform):

``` shell
molecule test --scenario-name logrotate --platform-name rockylinux9
```

By adding `--destroy never` to this command, the container will not be destroyed
once the tests are complete. Which lets you connect to the container directly
and manually tests things:

``` shell
molecule login  --scenario-name logrotate --host rockylinux9
```

## Release Workflow

1. Bump the version number in `galaxy.xml`
1. Write the `release_summary` in `changelogs/fragments/2.0.0.yml`
1. Generate the changelog, using [antsibull-changelog], by running
   `antsibull-changelog release`
1. Commit the result
1. Make an annotated tag, e.g.
1. Make an annotated tag (adapt to the collection you want to release), e.g.
   `git tag v2.0.0 -a [-s] -m 'dalibo.essential v2.0.0'`
1. Push to the upstream repository, the CI will handle publication to
   Ansible Galaxy.

[antsibull-changelog]: https://pypi.org/project/antsibull-changelog/
